# Modifications to antiword 0.37

Antiword is a tool for translating ms-word documents (pre-openXML) into
pure text and other formats.

It is an old program, not changed in years.

This repository has slightly modified version of antiword 0.37:

* The current Debian patches (0.37-11+b1) are applied.
* A few changes were made for building with MinGW on MS Windows.
* One change for processing some table types which were not handled
  well by the original program. The resulting table is mangled, but the
  textual data (which antiword 0.37 lost) is now extracted, so that it
  will be found by an indexer (Recoll).

This repo will probably not be much useful to anybody, but it fullfills my
obligations under the GPL, as I am distributing a binary version of this
code with the Windows version of Recoll

The code license is of course unchanged (GPL v2). See Docs/COPYING.

Copyright (C) 1989, 1991 Free Software Foundation, Inc.
                675 Mass Ave, Cambridge, MA 02139, USA
