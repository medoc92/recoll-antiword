#include "execdir.h"

#include <string.h>

static void getfather(char *path)
{
    char *slash = strrchr(path, '/');
    if (slash) {
        *(slash+1) = 0;
    }
}

const char *getsimple(const char *path)
{
    char *slash = strrchr(path, '/');
    if (slash == 0)
        return path;
    return slash + 1;
}


#ifdef _WIN32
#include <Shlwapi.h>
#pragma comment(lib, "shlwapi.lib")

size_t wchartoutf8(const wchar_t *in, size_t wlen, char *out, size_t outlen)
{
    char *buffer;
    int flags = WC_ERR_INVALID_CHARS;
    int bytes;
    
    if (in == 0) {
        return 0;
    }

    /* If input length was not given, compute it. */
    if (wlen == 0) {
        wlen = wcslen(in);
    }
    /* WideCharToMultiByte with 0 output buffer and length to get the actual needed length */
    flags = WC_ERR_INVALID_CHARS;
    bytes = ::WideCharToMultiByte(CP_UTF8, flags, in, wlen, nullptr, 0, nullptr, nullptr);
    if (bytes <= 0) {
        fwprintf(stderr, L"wchartoutf8: conversion error1 for [%s]\n", in);
        return 0;
    }
    /* If we were called to compute the length, return it. If we were called with a small buffer,
       return an error. */
    if (outlen == 0 || out == 0) {
        return bytes + 1;
    } else if (outlen < bytes + 1) {
        return 0;
    }

    /* Perform the conversion */
    bytes = ::WideCharToMultiByte(CP_UTF8, flags, in, wlen, out, outlen, nullptr, nullptr);
    if (bytes <= 0) {
        fprintf(stderr, "wchartoutf8: WideCharToMultiByte (actual) failed\n");
        return 0;
    }
    out[bytes] = 0;
    return bytes+1;
}

char *execdir_thisexecdir()
{
    wchar_t text[MAX_PATH];
    char *path;
    size_t pathlen;

    GetModuleFileNameW(NULL, text, MAX_PATH);
#ifdef NTDDI_WIN8_future
    PathCchRemoveFileSpec(text, MAX_PATH);
#else
    PathRemoveFileSpecW(text);
#endif
    pathlen = wchartoutf8(text, 0, 0, 0);
    if (pathlen == 0) {
        return 0;
    }
    path = (char *)malloc(pathlen);
    if (path)
        pathlen = wchartoutf8(text, 0, path, pathlen);
    return path;
}

#elif defined(__APPLE__)

char *execdir_thisexecdir()
{
    uint32_t size = 0;
    char *path;
    _NSGetExecutablePath(nullptr, &size);
    char *path = (char*)malloc(size+1);
    if (path) {
        char *slash;
        _NSGetExecutablePath(path, &size);
        getfather(path);
    }
    return path;
}

#else
#include <unistd.h>
#include <limits.h>
#include <string.h>

#ifdef DEBUG
#include <stdio.h>
#define DEBLOG(X) fprintf X
#else
#define DEBLOG(X)
#endif

char *argv0;

void execdir_setargv0(const char *av0)
{
    argv0 = strdup(av0);
}

/* https://stackoverflow.com/questions/606041/how-do-i-get-the-path-of-a-process-in-unix-linux*/
char *execdir_thisexecdir()
{
    char pathbuf[PATH_MAX+2];
    char *pathenv;
    DEBLOG((stderr, "argv0: [%s]\n", argv0));

    /* Works on Linux */
    ssize_t buff_len = readlink("/proc/self/exe", pathbuf, PATH_MAX - 1);    
    if (buff_len != -1) {
        getfather(pathbuf);
        return strdup(pathbuf);
    }

    if (argv0[0] == 0) {
        if (getcwd (pathbuf, PATH_MAX) == 0)
            return 0;
        strcat(pathbuf, "/");
        return strdup(pathbuf);
    }

    /* Try argv0 */
    if ((realpath(argv0, pathbuf)) && (access(pathbuf, F_OK) == 0)) {
        DEBLOG((stderr, "Using realpath(argv0)\n"));
        getfather(pathbuf);
        return strdup(pathbuf);
    }

    /* Current path ?? This would seem to assume that . is in the PATH so would be covered
       later. Not sure I understand the case */
    const char *cmdname = getsimple(argv0);
    if (getcwd (pathbuf, PATH_MAX - strlen(cmdname) -1) == 0)
        return 0;
    strcat (pathbuf, "/");
    strcat (pathbuf, cmdname);
    if (access(pathbuf, F_OK) == 0) {
        getfather(pathbuf);
        return strdup(pathbuf);
    }

    /* Try the PATH. */
    pathenv = getenv("PATH");
    if (pathenv != 0) {
        DEBLOG((stderr, "Using the PATH\n"));
        char *trdir;
        pathenv = strdup(pathenv);
        for (trdir = strtok(pathenv, ":"); trdir != 0; trdir = strtok(0, ":")) {
            strncpy(pathbuf, trdir, PATH_MAX);
            strncat(pathbuf, "/", PATH_MAX);
            strncat(pathbuf, cmdname, PATH_MAX);

            if (access(pathbuf, F_OK) == 0) {
                free (pathenv);
                getfather(pathbuf);
                return strdup(pathbuf);
            }
        }
        free(pathenv);
    }

    if (getcwd (pathbuf, PATH_MAX) == 0)
        return 0;
    strcat(pathbuf, "/");
    return strdup(pathbuf);
}

#ifdef TEST_MAIN
#include <stdio.h>
int main(int argc, char ** argv)
{
    char *exe;
    execdir_setargv0(argv[0]);
    exe = execdir_thisexecdir();
    if (exe) {
        printf("exe: [%s]\n", exe);
        free(exe);
        exit(0);
    } else {
        fprintf(stderr, "execdir_thisexecdir failed\n");
        exit(1);
    }
}
#endif /* TEST_MAIN */
#endif /* !_WIN32 && !__APPLE__ */
