#ifndef _EXECDIR_H_INCLUDED_
#define _EXECDIR_H_INCLUDED_

#include <stdlib.h>

/// Try to return the directory where this executable resides. On Linux needs main() to have called
/// execdir_setargv0()
/// The return value must be freed.
extern char *execdir_thisexecdir();

/// Linux/BSD: call this from main to set the value of argv[0]
extern void execdir_setargv0(const char *av0);

#endif /* _EXECDIR_H_INCLUDED_ */
